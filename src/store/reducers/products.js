import {
  REQUETS_PRODUCTS,
  SET_PRODUCTS,
  REQUEST_FAILURE,
  REQUEST_PRODUCT,
  SET_PRODUCT
} from "../actions/products";

const initialState = {
  products: [],
  page: 1,
  isLoading: true,
  hasMore: true
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case REQUETS_PRODUCTS:
      return {
        ...state,
        isLoading: true,
        page: action.page
      };
    case SET_PRODUCTS:
      return {
        ...state,
        isLoading: false,
        page: state.page + 1,
        products: [...state.products, ...action.products],
        hasMore: action.products.length > 0
      };
    case REQUEST_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
        products: []
      };
    case REQUEST_PRODUCT:
      return {
        ...state,
        isLoading: true,
      };
    case SET_PRODUCT:
      return {
        ...state,
        isLoading: false,
        product: action.product
      };
    default:
      return state;
  }
};

export default productReducer;

export const REQUETS_PRODUCTS = "@products/REQUETS_PRODUCTS";
export const SET_PRODUCTS = "@products/SET_PRODUCTS";
export const REQUEST_FAILURE = "@products/REQUEST_FAILURE";
export const REQUEST_PRODUCT = "@products/REQUETS_PRODUCT";
export const SET_PRODUCT = "@products/SET_PRODUCT";

export const requestsProducts = page => {
  return {
    type: REQUETS_PRODUCTS,
    page
  };
};

export const setProducts = products => ({
  type: SET_PRODUCTS,
  products
});

export const requestFail = error => ({
  type: SET_PRODUCTS,
  error
});

export const requestProduct = () => ({
  type: REQUEST_PRODUCT,
});

export const setProduct = product => ({
  type: SET_PRODUCT,
  product
});

import axios from "axios";
import {
  requestsProducts,
  setProducts,
  requestFail,
  requestProduct,
  setProduct
} from "../actions/products";

const endpoint = "https://assignment-appstreet.herokuapp.com/api/v1/";

export const getProducts = page => {
  console.log("page >> ", page, endpoint);
  return dispatch => {
    console.log("dispatching>>");
    dispatch(requestsProducts(page));
    console.log("sthunk");
    return axios
      .get(`${endpoint}products?page=${page}`)
      .then(res => {
        console.log("res.data", res.data);
        if (res.data === "") {
          dispatch(requestFail("Error"));
        } else {
          dispatch(setProducts(res.data.products));
        }
      })
      .catch(error => {
        console.log("error", error);

        dispatch(requestFail(error));
      });
  };
};

export const getProduct = productId => {
  return dispatch => {
    dispatch(requestProduct());
    return axios
      .get(`${endpoint}products/${productId}`)
      .then(res => {
        console.log("res.data", res.data);
        if (res.data === {}) {
          dispatch(requestFail("Error"));
        } else {
          dispatch(setProduct(res.data));
        }
      })
      .catch(error => {
        console.log("error", error);
        dispatch(requestFail(error));
      });
  };
};

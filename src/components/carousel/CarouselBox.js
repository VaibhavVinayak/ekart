import React from "react";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";

const CarouselBox = props => {
  console.log("carouselp", props);
  return (
    <CarouselProvider
      className="carousel-item"
      naturalSlideWidth={150}
      naturalSlideHeight={200}
      totalSlides={props.images ? props.images.length : 0}
      infinite
    >
      <Slider>
        {props.images &&
          props.images.length > 0 &&
          props.images.map((image, index) => (
            <Slide key={index} index={index}>
              <div
                style={{
                  background: `url('${image}') no-repeat`,
                  backgroundSize: "cover",
                  height: "400px"
                }}
              />
            </Slide>
          ))}
      </Slider>
    </CarouselProvider>
  );
};

export default CarouselBox;

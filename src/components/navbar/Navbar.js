import React from "react";
import { NavLink } from "react-router-dom";
import "./Navbar.css";

const Navbar = () => {
  return (
    <nav className="navbar">
      <div className="nav-wrapper">
        <NavLink className="left brand-logo" to="/">
          E-Kart
        </NavLink>
        <ul className="navbar-nav right">
          <li className="active">Home</li>
          <li className="waves-effect waves-light">About</li>
          <li className="waves-effect waves-light">Contact</li>
          <li className="waves-effect waves-light">Bag</li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;

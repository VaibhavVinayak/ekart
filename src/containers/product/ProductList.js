import React, { useLayoutEffect, useEffect } from "react";
import ProductListItem from "./ProductListItem";
import { getProducts } from "../../store/thunks/products";
import { connect } from "react-redux";
import Spinner from "../../components/spinner/Spinner";

const ProductList = props => {
  useEffect(() => {
    console.log("props", props);
    props.getProducts(props.page);
  }, []);
  useLayoutEffect(() => {
    window.addEventListener("scroll", onScroll, false);
    return () => window.removeEventListener("scroll", onScroll, false);
  });
  const onScroll = () => {
    if (
      window.innerHeight + document.documentElement.scrollTop ===
        document.documentElement.offsetHeight &&
      props.hasMore
    ) {
      props.getProducts(props.page);
    }
  };
  return (
    <div>
      <div className="flexible container">
        {props.products.map(product => (
          <ProductListItem
            key={product._id}
            id={product._id}
            name={product.name}
            imagesrc={product.images[0]}
          />
        ))}
      </div>
      <div style={{ display: "block", padding: "10px", textAlign: "center" }}>
        {props.isLoading ? <Spinner /> : ""}
        {!props.hasMore ? "Thats it..." : ""}
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  console.log(state);
  return {
    page: state.product.page,
    hasMore: state.product.hasMore,
    isLoading: state.product.isLoading,
    products: state.product.products
  };
};

const mapDispatchToProps = dispatch => ({
  getProducts: page => dispatch(getProducts(page))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);

import React from "react";
import "./Product.css";
import { Link } from "react-router-dom";

const ProductListItem = ({ id, imagesrc, name }) => {
  return (
    <div className="product-item">
      <Link to={"/products/" + id}>
        <div
          className="card product-card"
          style={{
            padding: "10px"
          }}
        >
          <div
            style={{
              background: `url('${imagesrc}') no-repeat`,
              backgroundSize: "cover",
              height: "200px"
            }}
          ></div>
          <div className="grid-item-title">
            <h6>{name}</h6>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default ProductListItem;

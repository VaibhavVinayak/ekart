import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import "../Product.css";
import { getProduct } from "../../../store/thunks/products";
import Spinner from "../../../components/spinner/Spinner";
import CarouselBox from "../../../components/carousel/CarouselBox";

const ProductDetail = props => {
  const [selectedVariation, setSelectedVariation] = useState({});
  useEffect(() => {
    props.getProduct(props.match.params.id);
  }, []);
  useEffect(() => {
    if (
      props.product &&
      props.product !== undefined &&
      props.product !== null
    ) {
      setSelectedVariation(props.product.product_variations[0]);
    }
  });
  console.log("props", props);
  console.log("selvar>>", selectedVariation);
  return (
    <div className="container">
      {props.isLoading === {} ? <Spinner /> : ""}
      {props.product === void 0 ? (
        ""
      ) : (
        <div className="card row product-detail-box">
          <div className="col s12 m6 carousel-container">
            <div className="card">
              <CarouselBox images={selectedVariation.images} />
            </div>
          </div>
          <div className="col s12 m6">
            <h6 className="product-detail-title">{selectedVariation.name}</h6>
            <div>
              <p>{props.product.primary_product.desc}</p>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    product: state.product.product,
    isLoading: state.product.isLoading
  };
};

const mapDispatchToProps = dispatch => ({
  getProduct: id => dispatch(getProduct(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
